[Latest commit before Discord Hack Week Deadline](https://gitlab.com/statmonkey/statmonkey-website/tree/20ebc4269f67ad3f9ab21102270923f0d8a953fe)

This is the only thing we didn't manage to finish by the deadline.  What it has:

- [x]Landing page
- [x]Working login
- [x]Data getting from the database

What is missing:

- [ ] Processing the data
- [ ] Properly showing the data
- [ ] Some page formatting issues