var w = $(window).width();

function httpGetAsync(theUrl) {
    return new Promise(resolve => {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
                resolve(JSON.parse(xmlHttp.responseText));
                console.log(JSON.parse (xmlHttp.responseText));
            }
        };

        xmlHttp.open("GET", theUrl, true); // true for asynchronous
        xmlHttp.send(null);
    });
}

let cache = [];
let uuid = new URL(window.location.href).searchParams.get('uuid');
httpGetAsync(`https://statmonkey.jamiestivala.com/api/information?uuid=${uuid}`).then((result) => {
    document.getElementById("username").innerHTML = result.userInfo.username;

    console.log(result);
    /*
    // document.getElementById("servername").innerHTML = guildname;

    for (let i = 0; i !== result.databaseInformation.length; i++) {
        let option = document.createElement("OPTION");
        if(result.databaseInformation[i].guildInfo.guild !== undefined){
            option.textContent = result.databaseInformation[i].guildInfo.guild.guild_name;

            let guildCache = new Cache(result.databaseInformation[i]);
            guildCache.messageChannelActivity();
            console.log(guildCache);

            cache.push(guildCache);
        }
        //option.value = result.databaseInformation[guild].guildInfo.guild.guild_id;
        //document.getElementById("server-list").appendChild(option);
        //option.innerHTML = result.databaseInformation[i].guildInfo.guild_name;

        //option.value = result.databaseInformation[i].guildInfo.guild.guild_id;
        //document.getElementById("server-list").appendChild(option);
    };
     */
});

function createCookie(key, value, date) {
    var expiration = new Date(date).toUTCString();
    var cookie = escape(key) + "=" + escape(value) + ";expires=" + expiration + ";";
    document.cookie = cookie;
    console.log(cookie);
    console.log("Creating new cookie with key: " + key + " value: " + value + " expiration: " + expiration);
}

function readCookie(name) {
    var key = name + "=";
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        while (cookie.charAt(0) === ' ') {
            cookie = cookie.substring(1, cookie.length);
        }
        if (cookie.indexOf(key) === 0) {
            return cookie.substring(key.length, cookie.length);
        }
    }
    return null;
}

var userHasCustom = readCookie("userHasCustom");

if (w < 600 && userHasCustom == "true") {
    var statsgrid = new Muuri('.statsgrid', { dragEnabled: false, layoutOnInit: false });
} else if (w >= 600 && userHasCustom == "true") {
    var statsgrid = new Muuri('.statsgrid', { dragEnabled: true, layoutOnInit: false });
} else if (w >= 600 && userHasCustom !== "true") {
    var statsgrid = new Muuri('.statsgrid', { dragEnabled: true });
} else if (w < 600 && userHasCustom !== "true") {
    var statsgrid = new Muuri('.statsgrid', { dragEnabled: false });
}

var counter = statsgrid.getItems();

var hiddenArray = [];

for (let j = 1; j <= counter.length; j++) {
    console.log(j);
    cookieRead = "hidden-state" + j;
    heldinfo = readCookie(cookieRead);
    hiddenArray.push(heldinfo);
}

console.log(hiddenArray);

var extravar = 0;
var setuperino = counter.length + 1;

for (let i = 1; i < setuperino; i++) {
    var griditem = document.getElementById("item" + i);
    var hiddenArrayCheck = hiddenArray[extravar];
    extravar = extravar + 1;
    if (hiddenArrayCheck == "1") {
        statsgrid.hide([griditem]);
    }
}

/*Defines initSort*/
var initSort = [];

/*Puts Values like item1, item2... in to initSort in order saved by user*/
for (let i = 0; i < counter.length; i++) {
    initSort.push(readCookie("sortpos" + i));
    console.log(initSort);
}

/*Puts initSort in to a string*/
var stringa = "";
for (let i = 0; i < initSort.length; i++) {
    stringa = stringa + initSort[i];
}

/*makes array of numbers in order*/
var arrayofPosAlpha = stringa.split("item");
console.log(arrayofPosAlpha);
/*removes empty first value*/
arrayofPosAlpha.splice(0, 1);
console.log(arrayofPosAlpha);


function gridOrder(order) {
    var currentItems = statsgrid.getItems();
    var currentItemIds = currentItems.map(item => item.getElement().getAttribute('data-id'));
    var newItems = [];
    var itemId;
    var itemIndex;

    for (var i = 0; i < order.length; i++) {
        itemId = order[i];
        itemIndex = currentItemIds.indexOf(itemId);
        if (itemIndex > -1) {
            newItems.push(currentItems[itemIndex])
        }
    }

    statsgrid.sort(newItems);
}

if (userHasCustom == "true") {
    gridOrder(arrayofPosAlpha);
}


var buttonstatus = 0;

function removeItem(itemnum) {
    var cross1 = document.getElementById('cross' + itemnum);
    var item1 = document.getElementById('item' + itemnum);
    cross1.addEventListener('click', function () {
        statsgrid.hide([item1]);
    });
    createCookie("hidden-state" + itemnum, 1, Date.UTC(2030, 8, 1));

}

function showItem(itemnum) {
    var cb1 = document.getElementById('cb' + itemnum);
    var item1 = document.getElementById('item' + itemnum);
    cb1.addEventListener('click', function () {
        statsgrid.show([item1]);
    });
    createCookie("hidden-state" + itemnum, 0, Date.UTC(2030, 8, 1));
}

function editButton() {
    if (buttonstatus == 0) {
        document.getElementById("bottommenu").style.bottom = "0px";
        document.getElementById("edit").innerHTML = "Save Changes";
        var crosses = document.getElementsByClassName("cross");
        for (let i = 0; i < crosses.length; i++) {
            crosses[i].style.display = "inherit";
        }
        buttonstatus = 1;
    } else {
        document.getElementById("bottommenu").style.bottom = "-260px";
        document.getElementById("edit").innerHTML = "Edit Grid";
        var crosses = document.getElementsByClassName("cross");
        for (let i = 0; i < crosses.length; i++) {
            crosses[i].style.display = "none";
        }
        buttonstatus = 0;
        var cookiedatacheck = true;
        createCookie("userHasCustom", cookiedatacheck, Date.UTC(2030, 8, 1));
        var order = statsgrid.getItems().map(item => item.getElement().getAttribute('id'));
        console.log(order);
        for (let i = 0; i < order.length; i++) {
            createCookie("sortpos" + i, order[i], Date.UTC(2030, 8, 1));
        }
    }

}


var CSS_COLOR_NAMES = ["Aqua", "Aquamarine", "Blue", "BlueViolet", "Brown", "BurlyWood", "CadetBlue", "Chartreuse", "Chocolate", "Coral", "CornflowerBlue", "Crimson", "Cyan", "DarkBlue", "DarkCyan", "DarkGoldenRod", "DarkGreen", "DarkKhaki", "DarkMagenta", "DarkOliveGreen", "Darkorange", "DarkOrchid", "DarkRed", "DarkSalmon", "DarkSeaGreen", "DarkSlateBlue", "DarkTurquoise", "DarkViolet", "DeepPink", "DeepSkyBlue", "DodgerBlue", "FireBrick", "ForestGreen", "Fuchsia", "Gainsboro", "Gold", "GoldenRod", "Gray", "Grey", "Green", "GreenYellow", "HotPink", "IndianRed", "Indigo", "Khaki", "LawnGreen", "LightBlue", "LightCoral", "LightGray", "LightGrey", "LightGreen", "LightPink", "LightSalmon", "LightSeaGreen", "LightSlateGray", "LightSteelBlue", "Lime", "LimeGreen", "Magenta", "Maroon", "MediumAquaMarine", "MediumBlue", "MediumOrchid", "MediumPurple", "MediumSeaGreen", "MediumSlateBlue", "MediumSpringGreen", "MediumTurquoise", "MediumVioletRed", "MidnightBlue", "MistyRose", "NavajoWhite", "Navy", "Olive", "OliveDrab", "Orange", "OrangeRed", "Orchid", "PaleGreen", "PaleTurquoise", "PaleVioletRed", "PeachPuff", "Peru", "Pink", "Plum", "PowderBlue", "Purple", "Red", "RosyBrown", "RoyalBlue", "SaddleBrown", "Salmon", "SandyBrown", "SeaGreen", "Sienna", "Silver", "SkyBlue", "SlateBlue", "SlateGray", "SlateGrey", "SpringGreen", "SteelBlue", "Tan", "Teal", "Thistle", "Tomato", "Turquoise", "Violet", "Yellow", "YellowGreen"];

/*Chart 1 Datasets*/
var colors1 = ["Dave", "Jamie", "Pickle", "Ellenajsjdj"];
var data1 = [200, 100, 750, 120];

/*Chart 2 Datasets*/
var colors2 = ["Dave", "Jamie", "Pickle", "Ellenajsjdj", "JEff1", "Pickleamans", "alanasn", "asd", "2wesd", "sadow"];
var data2 = [200, 100, 750, 120, 200, 100, 750, 120, 123, 230];

/*Chart 3 Datasets*/
var labels = ["00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"];
var data3 = ["10", "11", "12", "15", "12", "9", "10", "14", "20", "23", "25", "29", "34", "35", "45", "50", "52", "56", "58", "64", "75", "56", "50", "9"];

/*Chart 4 Datasets*/
var colors4 = ["Voicey boys", "Alan", "popsicle", "LukeIrridIkellmu"];
var data4 = ["100000", "312312", "312323", "900000"];

/*Chart 8 Datasets*/
var labels8 = ["Jun18", "Jul18", "Aug18", "Sep18", "Oct18", "Nov18", "Dec18", "Jan19", "Feb19", "Mar19", "Apr19", "May19"];
var data8 = [2, 3, 4, 14, 23, 25, 28, 27, 35, 56, 57, 59];

/*Generates Colors for Pie/Doughnut Charts*/
function randomColor() {
    var min = 0;
    var max = CSS_COLOR_NAMES.length;
    console.log(max);
    var random = Math.floor(Math.random() * (+max - +min)) + +min;
    return random;
}

function generateRandomColors(amount) {
    let generated = [];
    for (let i = 0; i !== amount; i++) {
        function generateColor() {
            let newlyGenerated = randomColor();
            for (let i = 0; i !== generated.length; i++) {
                if (CSS_COLOR_NAMES[newlyGenerated] === generated[i]) return generateColor();
            }
            return CSS_COLOR_NAMES[newlyGenerated];
        }

        generated.push(generateColor());
    }
    console.log(generated);
    return generated;
}

/*Assigning Colors to Graphs Randomly*/
var chosencolors1 = generateRandomColors(data1.length);
var chosencolors2 = generateRandomColors(data2.length);
var chosencolors3 = generateRandomColors(data3.length);
var chosencolors4 = generateRandomColors(data4.length);

var ctx = document.getElementById("chart1");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: colors1,
        datasets: [
            {
                data: data1,
                backgroundColor: chosencolors1,
                borderColor: "black"
            }
        ]
    }


}
);

var ctx2 = document.getElementById("chart2");
var myChart2 = new Chart(ctx2, {
    type: 'pie',
    data: {
        labels: colors2,
        datasets: [{
            data: data2,
            backgroundColor: chosencolors2,
            borderColor: "black"
        }]
    }

});

var ctx3 = document.getElementById("chart3");
var myChart3 = new Chart(ctx3, {
    type: 'bar',
    data: {
        labels: labels,
        datasets: [
            {
                label: "Average Users by Hour",
                data: data3,
                backgroundColor: chosencolors3,
                borderColor: "black",
                borderWidth: 1
            }
        ]
    }

});

var ctx4 = document.getElementById("chart4");
var myChart4 = new Chart(ctx4, {
    type: 'doughnut',
    data: {
        labels: colors4,
        datasets: [{
            data: data4,
            backgroundColor: chosencolors4,
            borderColor: "black"
        }]
    }
});

var ctx8 = document.getElementById("chart8");
var myChart8 = new Chart(ctx8, {
    type: 'line',
    data: {
        labels: labels8,
        datasets: [{
            data: data8,
            label: "No. of Server Users Over Time",
            borderColor: generateRandomColors(1),
            fill: false
        }]
    }
});

console.log("test");
class Cache {
    constructor(oneDatabaseInformation) {
        this.oneDatabaseInformation = oneDatabaseInformation;

        this.colors1 = []; //channels
        this.data1 = []; //amount of messages
    }

    messageChannelActivity() {
        if(this.oneDatabaseInformation.guildInfo === undefined) return;

        let messages = this.oneDatabaseInformation.guildInfo.allMessages;
        if(messages === undefined) return; //No messages have been logged yet
        let info = {};
        messages.forEach(message => {
            let channelId = message.channelId;
            console.log(channelId);
            if (info.hasOwnProperty(channelId) === false) {
                info[channelId] = {"channelName": message.channel_name, "amount": 1}
            } else {
                info[channelId].amount++;
            }
        });
        
        console.log(Object.keys(info));
        Object.keys(info).forEach(function(key,index) {
            this.colors1.push(index.channelName);
            this.data1.push(index.amount);
        });
    }
}